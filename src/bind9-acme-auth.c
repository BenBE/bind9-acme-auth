#include <ctype.h>
#include <errno.h>
#include <inttypes.h>
#include <netdb.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <unistd.h>

#include <arpa/inet.h>

#include <sys/socket.h>
#include <sys/types.h>
#include <sys/un.h>

/**
 * @brief External grant authenticator for BIND's named.
 *
 * @author Benny "BenBE" Baumann <BenBE@geshi.org>
 * @copyright (C) 2023 by Benny "BenBE" Baumann <BenBE@geshi.org>
 *
 * This is based on
 *   tserv.c (C)2020 by Jan-Piet Mens <jp@mens.de>
 *
 * This program creates a Unix domain socket and awaits
 * auth request packets from named(8) asking whether to
 * grant or deny a particular update.
 *
 * @see https://jpmens.net/2020/12/20/bind-named-grants-using-external-authenticator/
 * for details on the actual protocol.
 */

// The path to the socket to create
#define socket_path	"/run/bind9-acme-auth.socket"

static inline void err(const char* s)
{
    perror(s);
    exit(2);
}

void respond(int socket, int32_t response)
{
    int32_t response_netorder = htonl(response);

    ssize_t n = write(socket, &response_netorder, sizeof(response_netorder));
    if (0 > n) {
        perror("failed write");
    } else if (sizeof(response) != n) {
        errno = ENOMEM;
        perror("short write");
    }
}

uint32_t handle_request(
    const char* signer,
    const char* name,
    const char* addr,
    const char* rtype,
    const char* key,
    const char* tkey
) {
    const char* key_suffix = ".acme";
    const char* name_prefix = "_acme-challenge.";

    (void)tkey;

    // Check non-empty signer name
    if (!strlen(signer)) {
        return 0;
    }

    // Check non-empty DNS RR name
    if (!strlen(name)) {
        return 0;
    }

    // Check non-empty update client address
    if (!strlen(addr)) {
        return 0;
    }

    // Check we are updating a TXT DNS RR
    if (0 != strcasecmp(rtype, "TXT")) {
        return 0;
    }

    // Check non-empty TSIG Key name
    if (!strlen(key)) {
        return 0;
    }

    // Ensure no empty labels
    if (
        strstr(signer, "..") ||
        strstr(name, "..") ||
        strstr(key, "..") ||
        false
    ) {
        return 0;
    }

    // Ensure key starts with signer name followed by a suitable HMAC:
    // 163: HMAC-SHA256, 164: HMAC-SHA384, 165: HMAC-SHA512
    // Although permitted by the standard, HMAC-MD5, HMAC-SHA1 and HMAC-SHA224 are rejected
    if (strlen(key) < strlen(signer) + 6) {
        return 0;
    }
    if (0 != strncmp(signer, key, strlen(signer))) {
        return 0;
    }
    if (
        0 != strncmp(key + strlen(signer), "/163/", 5) && // HMAC-SHA256
        0 != strncmp(key + strlen(signer), "/164/", 5) && // HMAC-SHA384
        0 != strncmp(key + strlen(signer), "/165/", 5) && // HMAC-SHA512
        true
    ) {
        return 0;
    }

    // Check signer name has proper suffix
    if (strlen(signer) <= strlen(key_suffix)) {
        return 0;
    }
    if (0 != strncmp(signer + strlen(signer) - strlen(key_suffix), key_suffix, strlen(key_suffix) + 1)) {
        return 0;
    }

    // Check name prefix matches
    if (strlen(name) <= strlen(name_prefix)) {
        return 0;
    }
    if (0 != strncmp(name, name_prefix, strlen(name_prefix))) {
        return 0;
    }

    // Check name DNS RR is subdomain of domain encoded in signer
    if (strlen(name) <= strlen(signer) - strlen(key_suffix)) {
        return 0;
    }
    if (0 != strncmp(name + strlen(name) - (strlen(signer) - strlen(key_suffix)), signer, strlen(signer) - strlen(key_suffix))) {
        return 0;
    }
    if ('.' != name[strlen(name) - (strlen(signer) - strlen(key_suffix)) - 1]) {
        return 0;
    }

    return 1;
}

bool buf_check_remain(const void* buf, size_t bufsize, const void* src, size_t size)
{
    if ((const char*)src < (const char*)buf || (const char*)src > (const char*)buf + bufsize) {
        return false;
    }

    size_t maxlen = bufsize - ((const char*)src - (const char*)buf);
    return maxlen >= size;
}

const char* buf_check_strz(const void* buf, size_t bufsize, const char* src)
{
    if (src < (const char*)buf || src >= (const char*)buf + bufsize) {
        return NULL;
    }

    size_t maxlen = bufsize - (src - (const char*)buf);
    size_t len = strnlen(src, maxlen);
    if (len >= maxlen) {
        return NULL;
    }

    return src;
}

void serve(int s)
{
    uint8_t buf[1025];

    /*
     * Protocol version number (u32, network byte order, currently 1)
     * Request length (u32, network byte order)
     * Signer (null-terminated string)
     * Name (null-terminated string)
     * TCP source address (null-terminated string)
     * Rdata type (null-terminated string)
     * Key (null-terminated string)
     * TKEY token length (u32, network byte order)
     * TKEY token (remainder of packet)
     */

    ssize_t nbytes = read(s, buf, sizeof(buf) - 1);
    if (nbytes < 17) {
        respond(s, 2);
        return;
    }

    char *bp = (char*)buf;
    size_t buflen = (size_t)nbytes;

    buf[buflen] = 0; // Enforce NULL-termination
    buf[sizeof(buf) - 1] = 0; // Enforce NULL-termination

    if (!buf_check_remain(buf, buflen, bp, sizeof(uint32_t))) {
        respond(s, 2);
        return;
    }
    uint32_t proto = ntohl(*(uint32_t *)bp);
    bp += sizeof(uint32_t);
    if (proto != 1) {
        respond(s, 2);
        return;
    }

    if (!buf_check_remain(buf, buflen, bp, sizeof(uint32_t))) {
        respond(s, 2);
        return;
    }
    uint32_t plen = ntohl(*(uint32_t *)bp);
    bp += sizeof(uint32_t);
    if (plen != buflen) {
        respond(s, 2);
        return;
    }

    const char *signer = buf_check_strz(buf, buflen, bp);
    if (!signer) {
        respond(s, 2);
        return;
    }
    bp += strlen(signer) + 1;

    const char *name = buf_check_strz(buf, buflen, bp);
    if (!name) {
        respond(s, 2);
        return;
    }
    bp += strlen(name) + 1;

    const char *addr = buf_check_strz(buf, buflen, bp);
    if (!addr) {
        respond(s, 2);
        return;
    }
    bp += strlen(addr) + 1;

    const char *rtype = buf_check_strz(buf, buflen, bp);
    if (!rtype) {
        respond(s, 2);
        return;
    }
    bp += strlen(rtype) + 1;

    const char *key = buf_check_strz(buf, buflen, bp);
    if (!key) {
        respond(s, 2);
        return;
    }
    bp += strlen(key) + 1;

    /* TKEY len and TKEY follow here, but I'm uninterested */
    if (!buf_check_remain(buf, buflen, bp, sizeof(uint32_t))) {
        respond(s, 2);
        return;
    }
    uint32_t tlen = ntohl(*(uint32_t *)bp);
    bp += sizeof(uint32_t);
    if (!buf_check_remain(buf, buflen, bp, tlen)) {
        respond(s, 2);
        return;
    }

    const char *tkey = bp; // Not NULL-terminated … (ensured on our side)

    bp += tlen;

    // Check for end of (used) buffer
    if (buf_check_remain(buf, buflen, bp, 1)) {
        respond(s, 2);
        return;
    }

    /* Example output:
     *
     * Request len=76
     * 	signer=acme,
     * 	name=www.example.net.certs.example,
     * 	addr=127.0.0.2,
     * 	rtype=TXT,
     * 	key=acme/163/59864
     */

    /* Set permit = {0|1} to deny or permit respectively */
    uint32_t permit = handle_request(signer, name, addr, rtype, key, tkey);

    printf("Request len=%" PRIu32 " signer=%s, name=%s, addr=%s, rtype=%s, key=%s, tkey=%s, result=%s\n",
        plen, signer, name, addr, rtype, key, tkey, permit ? "grant" : "reject");

    respond(s, permit);
}

int main(int argc, char **argv)
{
    (void)argc;
    (void)argv;

    if (access(socket_path, F_OK) == 0) {
        if (unlink(socket_path) < 0) {
            err(socket_path);
        }
    }

    int s = socket(AF_UNIX, SOCK_STREAM, 0);
    if (s < 0) {
        err("socket");
    }

    struct sockaddr_un addr = { .sun_family = AF_UNIX };
    strncpy(addr.sun_path, socket_path, sizeof(addr.sun_path) - 1);

    if (bind(s, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
        err("bind");
    }

    if (listen(s, 5) < 0) {
        err("listen");
    }

    while (1) {
        socklen_t addrlen = sizeof(addr);
        int ns = accept(s, (struct sockaddr *)&addr, &addrlen);
        if (ns < 0) {
            err("accept");
        }

        serve(ns);
        close(ns);
    }
}
